# django-channels-tut

### Simple Chat Server made using Django-Channels

Steps to recreate it for yourself:
1) create a new directory
2) create a virtualenv using pipenv
3) clone this repo using:
    `git clone https://gitlab.com/sumit4613/django-channels-tut.git`
4) cd into django-channels-tut   

5) use `pipenv install` to install the dependencies
 
6) open a port for redis using
    `docker run -p 6379:6379 -d redis:3.2`
7) cd into mysite directory

8) run `python manage.py runserver`

To test whether this server working or not:

1) open 2 browsers

2) open `http://127.0.0.1:8000/chat/` in both the browsers and enter same room name in both browsers

3) then type anything you like.
